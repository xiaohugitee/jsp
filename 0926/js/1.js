window.onload = () => {
    const username = document.querySelector('#username')
    const username_error = document.querySelector('#username_error')
    const phone = document.querySelector('#phone')
    const phone_error = document.querySelector('#phone_error')
    const sub = document.querySelector('#sub')
    const left = document.querySelector('#left')
    const right = document.querySelector('#right')
    const time = document.querySelector('#time')

    setInterval(() => {
        let date = new Date()
        let year = date.getFullYear()
        let month = date.getMonth() + 1
        let day = date.getDate()
        let hour = date.getHours()
        let minute = date.getMinutes()
        let second = date.getSeconds()
        if (month < 10) {
            month = '0' + month
        }
        if (day < 10) {
            day = '0' + day
        }
        if (hour < 10) {
            hour = '0' + hour
        }
        if (minute < 10) {
            minute = '0' + minute
        }
        if (second < 10) {
            second = '0' + second
        }
        time.innerHTML = `${year}年${month}月${day}日 ${hour}:${minute}:${second}`
    }, 16)


    username.onblur = function () {
        const usernameValue = username.value
        if (usernameValue.length < 7 || usernameValue.length > 14) {
            //修改透明度 0-1 0完全透明 1完全不透明 做动画
            username_error.style.opacity = 1
            // username_error.style.visibility = 'visible'
            //边框变红
            username.style.border = '1px solid red'
            // 文本框抖动
            username.style.animation = 'shake 0.5s'
            //动画结束后清除动画
            setTimeout(() => {
                username.style.animation = ''
            }, 500)
            return
        }
        username_error.style.opacity = 0
        //边框变绿
        username.style.border = '1px solid green'
    }
    phone.onfocus = function () {
        left.style.backgroundImage = 'url("./images/22_close.0efad8c4.png")'
        right.style.backgroundImage = 'url("./images/33_close.eea03c39.png")'
    }
    phone.onblur = function () {
        const phoneValue = phone.value
        //手机号必须为11位 数字
        if (phoneValue.length !== 11 || isNaN(phoneValue)) {
            phone_error.style.opacity = 1
            //边框变红
            phone.style.border = '1px solid red'
            // 文本框抖动
            phone.style.animation = 'shake 0.5s'
            //动画结束后清除动画
            setTimeout(() => {
                phone.style.animation = ''
            }, 500)
            return
        }
        phone_error.style.opacity = 0
        //边框变绿
        phone.style.border = '1px solid green'
        left.style.backgroundImage = 'url("./images/22_open.72c00877.png")'
        right.style.backgroundImage = 'url("./images/33_open.43a09438.png")'

    }


    sub.addEventListener('click', (e) => {
        //阻止表单默认事件
        e.preventDefault()
        //用户名要求7-14位，手机号必须为11位数字(15分)
        const usernameValue = username.value
        const phoneValue = phone.value
        if (usernameValue.length < 7 || usernameValue.length > 14) {
            username_error.style.opacity = 1
            //边框变红
            username.style.border = '1px solid red'
            // 文本框抖动
            username.style.animation = 'shake 0.5s'
            //抖动结束后清除动画
            username.addEventListener('animationend', () => {
                username.style.animation = ''
            }
            )
        }
        if (phoneValue.length !== 11 || isNaN(phoneValue)) {
            phone_error.style.opacity = 1
            //边框变红
            phone.style.border = '1px solid red'
            // 文本框抖动
            phone.style.animation = 'shake 0.5s'
            //抖动结束后清除动画
            phone.addEventListener('animationend', () => {
                phone.style.animation = ''
            }
            )
        }
        if (usernameValue.length >= 7 && usernameValue.length <= 14 && phoneValue.length === 11 && !isNaN(phoneValue)) {
            location.href = './2.html'
        }

    })


}
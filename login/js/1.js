window.onload = () => {
    // 获取username输入框
    const username = document.querySelector('#username')
    window.username = username
    // 获取password输入框
    const password = document.querySelector('#password')
    // 获取登录按钮
    const loginBtn = document.querySelector('#loginBtn')
    // 获取错误提示
    const error1 = document.querySelector('#error-username')
    const error2 = document.querySelector('#error-password')
    username.onblur = () => {
        // 获取输入框的值
        const usernameValue = username.value
        // 判断输入框的值是否为空
        if (usernameValue === '') {
            error1.style.visibility = 'visible'
            return
        }
        error1.style.visibility = 'hidden'
    }
    password.onblur = () => {
        // 获取输入框的值
        const passwordValue = password.value
        // 判断输入框的值是否为空
        if (passwordValue === '') {
            error2.style.visibility = 'visible'
            return
        }
        error2.style.visibility = 'hidden'
    }
    loginBtn.onclick = (e) => {
        // 阻止表单的默认提交行为
        e.preventDefault()
        // 获取输入框的值
        const usernameValue = username.value
        const passwordValue = password.value
        // 判断输入框的值是否为空
        if (usernameValue === '') {
            error1.style.visibility = 'visible'
            alert('请输入用户名')
            return
        }
        if (passwordValue === '') {
            error2.style.visibility = 'visible'
            alert('请输入密码')
            return
        }
        //进行本地存储
        localStorage.setItem('username', usernameValue)
        localStorage.setItem('password', passwordValue)
        //跳转页面
        location.href = './2.html'
    }

}
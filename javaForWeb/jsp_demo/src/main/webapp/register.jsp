<%--
  Created by IntelliJ IDEA.
  User: humeng
  Date: 2022/11/21
  Time: 15:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>register</title>
    <style>
        :root{
            --primary-color:#6cf;
        }

        body{
            color: var(--primary-color);
        }
        .container{
            width: 500px;
            margin: 100px auto;
            text-align: center;
            background-color: beige;
            padding: 10px;
            border-radius: 40px;
            box-shadow: 1px 1px 4px rgba(0,0,0,.3);
        }
        .inp{
            width: 80%;
            height: 50px;
            margin-bottom: 10px;
            border: 1px solid var(--primary-color);
            border-radius: 25px;
            padding: 0 20px;
            outline: none;
        }
        .btn{
            border: 0;
            background-color: var(--primary-color);
            width: 150px;
            height: 50px;
            color: #fff;
            font-size: 22px;
            line-height: 50px;
            border-radius: 12px;
        }
        a{
            text-decoration: none;
            font-size: 14px;
            height: 30px;
            color: #666;
        }
        .primary-color{
            color: var(--primary-color);
            font-size: 20px;
        }
    </style>
</head>
<body>
<div class="container">
    <h2>注册</h2>
    <form action="/register" method="post">
        用户名:<input class="inp" type="text" required name="username">
        <br>
        密码: <input class="inp" type="password" required name="password">
        <br>
        手机号: <input class="inp" type="tel" name="phone" required>
        <br>
        性别: <input type="radio" name="gender" value="1" required> 男
        <input type="radio" name="gender" value="0" required> 女
        <br>
        生日: <input class="inp" type="date" name="userBirth" required>
        <br>
        <input class="btn" type="submit" value="注册">
    </form>

    <a href="./login.jsp"><span class="primary-color">去登录</span></a>
</div>

</body>
</html>

<%@ page import="com.red.connetcion.Result" %>
<%@ page import="java.util.Objects" %>
<%@ page import="cn.hutool.core.util.StrUtil" %>
<%@ page import="java.sql.Date" %><%--
  Created by IntelliJ IDEA.
  User: humeng
  Date: 2022/11/21
  Time: 15:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>v</title>
</head>
<%
    request.setCharacterEncoding("utf-8");

%>
<body>
<jsp:useBean id="userController" class="com.red.controller.UserController" scope="session"/>


<%
    String username = request.getParameter("username");
    String password = request.getParameter("password");
    if (StrUtil.isBlank(username)||StrUtil.isBlank(password)){
        out.println("<script>alert(\"请输入内容\");location.href = \"login.jsp\";</script>");
        return;
    }
    Result result = userController.login(username, password);
    if (Objects.isNull(result)){
        out.println("<script>alert(\"系统错误\");location.href = \"login.jsp\";</script>");
        return;
    }
    if (result.getSuccess()){
        session.setAttribute("loginInfo",result);
        out.println("<script>alert(\"登陆成功\");location.href = \"index.jsp\";</script>");
        return;
    }
%>
<jsp:useBean id="user" class="com.red.dao.User" scope="page"/>
<jsp:setProperty name="user" property="username" param="username"/>
<jsp:setProperty name="user" property="password" param="password"/>
<jsp:setProperty name="user" property="gender" param="gender"/>
<jsp:setProperty name="user" property="phone" param="phone"/>
<%
    String userBirth = request.getParameter("userBirth");
    Date userDate = Date.valueOf(userBirth);
    user.setUserBirth(userDate);
%>
<%

    if (Objects.isNull(user.getUserBirth())){
//        则不是注册的
        out.println("<script>alert(\"登陆失败,用户名或密码错误或没有该用户\");location.href = \"login.jsp\";</script>");
        return;
    }
//    注册业务
    Result register = userController.register(user);
    if (register.getSuccess()){
        out.println("<script>alert(\"注册成功,请登陆\");location.href = \"login.jsp\";</script>");
    }else{
        String msg = register.getMsg();
        out.println("<script>alert(\""+msg+"\");location.href = \"register.jsp\";</script>");

    }


%>


</body>
</html>

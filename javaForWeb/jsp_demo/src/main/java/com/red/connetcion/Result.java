package com.red.connetcion;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result {
    private Boolean success;
    private Object data;
    private String msg;
    public static Result ok(Object data){
        return new Result(true,data,"");
    }
    public static Result ok(String msg){
        return new Result(true,null,msg);
    }
    public static Result ok(Object data,String msg){
        return new Result(true,data,msg);
    }
    public static Result fail(String msg){
        return new Result(false,null,msg);
    }
}

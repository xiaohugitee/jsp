package com.red.connetcion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * 单例模式(饿汉式单例) 获取数据库连接对象
 */
public class DriverConnectionUtil {
    private static final Connection connection;
    static {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db1", "root", "12345678");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    /**
     * 私有构造器 防止被创建
     */
    private DriverConnectionUtil(){ }
    /**
     * 获取jdbc连接
     * @return Connection
     */
    public static Connection getConnection(){
        return connection;
    }
}

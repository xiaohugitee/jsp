package com.red.filter;

import com.red.connetcion.Result;

import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

@WebFilter(filterName = "LoginFilter",
        value = {"/index.jsp"
                /*,"/register.jsp"*/})
public class LoginFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        Result loginInfo = (Result) httpServletRequest.getSession().getAttribute("loginInfo");
        if (Objects.isNull(loginInfo) || !loginInfo.getSuccess()){
            httpServletResponse.sendRedirect("/login.html");
            return;
        }
        chain.doFilter(request, response);
    }
    public void init(FilterConfig config) throws ServletException {
    }

    public void destroy() {
    }


}

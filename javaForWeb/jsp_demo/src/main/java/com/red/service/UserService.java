package com.red.service;


import com.red.dao.User;
import com.red.connetcion.Result;

public interface UserService {
    Result login(String username, String password);

    Result register(User user);

}

package com.red.controller;

import com.red.dao.User;
import com.red.service.UserService;
import com.red.service.impl.UserServiceImpl;
import com.red.connetcion.Result;

public class UserController {
    private final UserService userService = new UserServiceImpl();
    public Result login(String username, String password){
        return userService.login(username, password);
    }

    public Result register(User user){
        return userService.register(user);
    }
}

package com.red.mapper;

import com.red.dao.User;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {
    User login(@Param("username") String username, @Param("password") String password);

    void register(User user);

}

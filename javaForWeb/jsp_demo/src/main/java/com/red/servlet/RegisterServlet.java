package com.red.servlet;

import com.red.connetcion.Result;
import com.red.controller.UserController;
import com.red.dao.User;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Date;


@WebServlet(name = "RegisterServlet", value = "/register")
public class RegisterServlet extends HttpServlet {
    private final UserController userController = new UserController();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String gender = request.getParameter("gender");
        String phone = request.getParameter("phone");
        String userBirth = request.getParameter("userBirth");
        Date userDate = Date.valueOf(userBirth);
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setGender("1".equals(gender)?'1':'0');
        user.setPhone(phone);
        user.setUserBirth(userDate);

        Result register = userController.register(user);
        if (register.getSuccess()){
            response.getWriter().write("<script>alert(\"注册成功,请登陆\");location.href = \"login.jsp\";</script>");
        }else{
            String msg = register.getMsg();
            response.getWriter().write("<script>alert(\""+msg+"\");location.href = \"register.jsp\";</script>");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request,response);
    }
}

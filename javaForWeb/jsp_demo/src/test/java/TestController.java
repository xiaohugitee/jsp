import com.red.dao.User;
import com.red.service.UserService;
import com.red.service.impl.UserServiceImpl;
import com.red.connetcion.Result;
import org.junit.Test;

import java.sql.Date;

public class TestController {
    private final UserService userService = new UserServiceImpl();
    @Test
    public void testLogin(){
        Result result = userService.login("admin", "123456");
        System.out.println(result);
    }
    @Test
    public void testRegister(){
        User user = new User(null, "test3", "123456", "18888888888", '1', Date.valueOf("2001-10-10"));
        Result register = userService.register(user);
        System.out.println(register);
    }
}

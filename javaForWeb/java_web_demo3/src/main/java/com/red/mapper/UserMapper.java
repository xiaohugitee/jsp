package com.red.mapper;

import com.red.entity.User;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {
    int login( User user);
}

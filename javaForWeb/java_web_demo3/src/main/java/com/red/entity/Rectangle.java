package com.red.entity;

public class Rectangle {
    private double length;

    private double width;

    public Rectangle() {
        length = 20;
        width = 10;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    //定义求面积的方法
    public double computerArea(){
        return this.length*this.width;
    }
    //定义求周长的方法
    public double coumputerC(){
        return 2*(this.length+this.width);

    }
}

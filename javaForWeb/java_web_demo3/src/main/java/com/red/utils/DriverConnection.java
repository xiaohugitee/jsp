package com.red.utils;

import java.sql.Connection;
import java.sql.DriverManager;

public class DriverConnection {
    public static  Connection connection;
    static {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql:///db1", "root", "12345678");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static Connection getConnection(){
        return connection;
    }
}

package com.red.utils;

import lombok.Data;

@Data
public class Result {
    private Boolean success;
    private Object data;
    private String msg;

    public Result() {
    }

    public Result(Boolean success, Object data, String msg) {
        this.success = success;
        this.data = data;
        this.msg = msg;
    }

    public static Result ok(Object data){
        return new Result(true,data,"");
    }

    public static Result fail(String msg){
        return new Result(false,null,msg);
    }

}

<%@ page import="java.util.Random" %><%--
  Created by IntelliJ IDEA.
  User: humeng
  Date: 2022/10/12
  Time: 11:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%
    Random random = new Random();
    int number = random.nextInt(100);
//    偶数
    if (number%2==0){
%>
<%--<jsp:forward page="even.jsp">--%>
<%--    <jsp:param name="num" value="<%=number%>"/>--%>
<%--</jsp:forward>--%>
<jsp:forward page="even.jsp">
    <jsp:param name="randomNumber" value="<%=number%>"/>
</jsp:forward>
<%
    }else {
%>
<jsp:forward page="odd.jsp">
    <jsp:param name="randomNumber" value="<%=number%>"/>
</jsp:forward>
<%
    }
%>
</body>
</html>

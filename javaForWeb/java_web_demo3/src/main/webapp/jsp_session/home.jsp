<%@ page import="java.util.Objects" %><%--
  Created by IntelliJ IDEA.
  User: humeng
  Date: 2022/10/26
  Time: 10:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>home</title>
</head>
<body>
<%
    String username = (String) session.getAttribute("username");
    String number = (String) session.getAttribute("number");
    String password = (String) session.getAttribute("password");
    if (Objects.isNull(username)||Objects.isNull(number)||Objects.isNull(password)){
        out.println("<script>alert(\"没有权限登陆\");window.location = \"login.jsp\"</script>");
    }
    out.println("欢迎登陆;<br>姓名:"+username+", "+"学号:"+number);
%>
</body>
</html>

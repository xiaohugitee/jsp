<%--
  Created by IntelliJ IDEA.
  User: humeng
  Date: 2022/10/19
  Time: 09:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>index</title>
</head>
<body>
<ul>
    <li>
        class1:/jsp_include/
        <ul>
            <li>
                <a href="./jsp_include/index.jsp">index.jsp</a>
            </li>
        </ul>
    </li>
    <li>
        class2:/jsp_forward/
        <ul>
            <li>
                <a href="./jsp_forward/test.jsp">test.jsp</a>
            </li>
        </ul>
    </li>
    <li>
        class3:/jsp_req_res/
        <ul>
            <li>
                <a href="./jsp_req_res/1.jsp">1.jsp</a>
            </li>
        </ul>
    </li>
    <li>
        class4:/jsp_cookies_session/
        <ul>
            <li>
                <a href="./jsp_cookies_session/1.jsp">1.jsp</a>
            </li>
        </ul>
    </li>
    <li>
        class5:/jsp_session/
        <ul>
            <li>
                <a href="./jsp_session/login.jsp">login.jsp</a>
            </li>
        </ul>
    </li>
    <li>
        class6:/jsp_application/
        <ul>
            <li>
                <a href="./jsp_application/1.jsp">1.jsp</a>
            </li>
        </ul>
    </li>

    <li>
        class7:/jsp_mysql/
        <ul>
            <li>
                <a href="./jsp_mysql/login.jsp">login.jsp</a>
            </li>
        </ul>
    </li>

    <li>
        class8:/jsp_javaBean/
        <ul>
            <li>
                <a href="./jsp_javaBean/1.jsp">1.jsp</a>
            </li>
        </ul>
    </li>
    <li>
        class9:/jsp_servlet/
        <ul>
            <li>
                <a href="./jsp_servlet/login.html">login.html</a>
            </li>
        </ul>
    </li>
</ul>

<ul>
    <li>
        简易聊天室(基于jsp的application和session实现)
        <ul>
            <li>
                <a href="./jsp_application_demo/login.jsp">login.jsp</a>
            </li>
        </ul>
    </li>
</ul>

</body>
</html>

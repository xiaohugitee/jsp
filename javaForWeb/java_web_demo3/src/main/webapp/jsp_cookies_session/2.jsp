<%@ page import="java.nio.charset.StandardCharsets" %>
<%--
  Created by IntelliJ IDEA.
  User: humeng
  Date: 2022/10/24
  Time: 14:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>2</title>
</head>
<body>
<%
  request.setCharacterEncoding("utf-8");
  String username1 = request.getParameter("username");
  String password = request.getParameter("password");
  username1 = new String(username1.getBytes(StandardCharsets.ISO_8859_1),StandardCharsets.UTF_8);

  HttpSession session1 = request.getSession();

  //设置session的最长的请求间隔 默认为30分钟 单位秒
//  session1.setMaxInactiveInterval(30*60);

  //使session立即失效
//  session1.invalidate();

  session1.setAttribute("username",username1);
  session1.setAttribute("password",password);
  response.sendRedirect("3.jsp");
%>

</body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: humeng
  Date: 2022/11/16
  Time: 10:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%
    request.setCharacterEncoding("utf-8");
%>

<%--<jsp:useBean id="car" class="com.red.entity.Car" scope="page"/>--%>
<jsp:useBean id="car" class="com.red.entity.Car" scope="session"/>

<%--<jsp:setProperty name="car" property="brand" param="brand"/>--%>
<%--<jsp:setProperty name="car" property="car" param="car"/>--%>

<%--自动填充 不过类型要匹配上 都得是String--%>
<jsp:setProperty name="car" property="*"/>

汽车品牌: <jsp:getProperty name="car" property="brand"/>
<br>
汽车车牌号: <jsp:getProperty name="car" property="car"/>

<a href="./getSession.jsp">查看car的session信息</a>

</body>
</html>

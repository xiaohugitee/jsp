<%@ page import="com.red.entity.Rectangle" %>
<%--
  Created by IntelliJ IDEA.
  User: humeng
  Date: 2022/11/9
  Time: 10:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>1</title>
</head>
<body>
<%--TODO:动作标记useBean:时用来查找或者实例化一个JavaBean--%>
<jsp:useBean id="rectangle" class="com.red.entity.Rectangle" scope="page"/>
<%--scope为有效的范围:page(缺省),request,session,application--%>
<%--TODO:jsp:setProperty 设置bean的属性值--%>
<jsp:setProperty name="rectangle" property="length" value="4"/>
<jsp:setProperty name="rectangle" property="width" value="3"/>

<%--TODO:jsp:getProperty获取bean的属性--%>
<%--<p>获取矩形的长:<%=rectangle.getLength()%></p>--%>
获取矩形的长:<jsp:getProperty name="rectangle" property="length"/>
<%--<p>获取矩形的宽:<%=rectangle.getWidth()%></p>--%>
获取矩形的宽:<jsp:getProperty name="rectangle" property="width"/>

<p>获取矩形的面积:<%=rectangle.computerArea()%></p>
<p>获取矩形的周长:<%=rectangle.coumputerC()%></p>





</body>
</html>

<%@ page import="java.nio.charset.StandardCharsets" %>
<%@ page import="java.util.Arrays" %><%--
  Created by IntelliJ IDEA.
  User: humeng
  Date: 2022/10/17
  Time: 14:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>2</title>
</head>
<body>
<%
  //1、解决乱码:POST。
  // POST底层是通过getReader() 字符输入流获取数据，但是tomcat默认的获取流的数据的编码是ISO-8859-1的 所以读中文数据的时候乱码
  request.setCharacterEncoding("utf-8");

  String username = request.getParameter("username");
  String password = request.getParameter("password");
  String keyword = request.getParameter("keyword");
  String[] hobbies = request.getParameterValues("hobbies");
  if (keyword!=null){
    keyword = new String(keyword.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
    out.println(keyword);
  }
  if (hobbies!=null){
    for (int i = 0; i < hobbies.length; i++) {
      String hobby = hobbies[i];
      hobbies[i] = new String(hobby.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
    }
    out.println(Arrays.toString(hobbies));
  }

  if (username!=null){
    byte[] bytes = username.getBytes(StandardCharsets.ISO_8859_1);
    username = new String(bytes, StandardCharsets.UTF_8);
    out.println("用户名是:"+username);
  }

  out.println("<br>");
  if (password!=null)
    out.println("密码是:"+password);
%>

</body>
</html>

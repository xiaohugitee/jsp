<%@ page import="java.util.Arrays" %><%--
  Created by IntelliJ IDEA.
  User: humeng
  Date: 2022/10/17
  Time: 15:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>4</title>
</head>
<body>
<%
  request.setCharacterEncoding("utf-8");

  String[] fs = request.getParameterValues("f");
  if (fs!=null){
    out.println(Arrays.toString(fs));
  }

//  request.getRequestDispatcher("2.jsp").forward(request,response); 转发 url地址不变 由服务器完成

//  response.setHeader(); 设置响应头
//  response.sendRedirect(); 重定向 url地址改变 由浏览器完成
//  response.setContentType("text/html;charset=UTF-8"); 改变contentType
//  response.getWriter().write("<h1>水果</h1>"); 写入到页面内容
%>

</body>
</html>

<%@ page import="java.sql.*" %>
<%@ page import="com.red.utils.DriverConnection" %>
<%@ page import="java.util.Objects" %><%--
  Created by IntelliJ IDEA.
  User: humeng
  Date: 2022/11/2
  Time: 10:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>3</title>
</head>
<body>
<%
    String username = (String) session.getAttribute("username");
    if (Objects.isNull(username)){
        out.println("<script>alert(\"请先登陆\");location.href = \"login.jsp\";</script>");
    }
%>
<h1>
    <%=session.getAttribute("username")+"进入首页"%>
</h1>
<form action="4.jsp" method="post">
    名称:<input type="text" name="name">
    价格:<input type="text" name="price">
    种类:<input type="text" name="category">
    <input type="submit" value="ok">
</form>
<%
    try {
//        Class.forName("com.mysql.cj.jdbc.Driver");
//        Connection connection = DriverManager.getConnection("jdbc:mysql:///db1", "root", "12345678");
        Connection connection = DriverConnection.getConnection();
        Statement statement = connection.createStatement();

        ResultSet resultSet = statement.executeQuery("select * from goods_info;");
        out.println("<table border = '1' cellspacing='0'>");
        out.println("<tr>");
        out.println("<td>id编号</td>" +
                "<td>名称</td>" +
                "<td>价格</td>" +
                "<td>种类</td>");
        out.println("</tr>");
        while (resultSet.next()){
            out.println("<tr>");
            out.println("<td>"+resultSet.getInt("id")+"</td>");
            out.println("<td>"+resultSet.getString("name")+"</td>");
            out.println("<td>"+resultSet.getDouble("price")+"</td>");
            out.println("<td>"+resultSet.getString("category")+"</td>");
            out.println("</tr>");
        }
        out.println("</table>");

    } catch (Exception e) {
        throw new RuntimeException(e);
    }
%>




</body>
</html>

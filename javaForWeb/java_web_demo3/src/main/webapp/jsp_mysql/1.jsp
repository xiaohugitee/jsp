<%@ page import="java.sql.*" %>
<%@ page import="com.red.utils.DriverConnection" %>
<%@ page import="java.util.Objects" %><%--
  Created by IntelliJ IDEA.
  User: humeng
  Date: 2022/10/31
  Time: 14:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>1</title>
</head>
<body>
<%
    request.setCharacterEncoding("utf-8");
    String username = request.getParameter("username");
    //sql注入问题 'or '1' = '1
    // 'or '1' = '1
    //select * from user where username = '' and password = '' or '1' = '1';
    String password = request.getParameter("password");
    if ("".equals(username)||"".equals(password)|| Objects.isNull(username)||Objects.isNull(password)){
        out.println("<script>alert(\"用户名和密码不能为空\");location.href = \"login.jsp\";</script>");
        return;
    }
    try {
        //1.加载数据库驱动
//        Class.forName("com.mysql.cj.jdbc.Driver");
        //2.建立连接对象
//        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db1", "root", "12345678");
        Connection connection = DriverConnection.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("select * from user where username = ? and password = ?;");
        preparedStatement.setString(1,username);
        preparedStatement.setString(2,password);
        ResultSet resultSet = preparedStatement.executeQuery();
        //插入
//        String sql1 = "insert into user (username, password) values ('"+username+"','"+password+"');";
        if (resultSet.next()){
            session.setAttribute("username",username);
            session.removeAttribute("msg");
            out.println("<script>alert(\"登陆成功\");location.href = \"3.jsp\";</script>");
        }else {
//            statement.executeUpdate(sql1);
            session.setAttribute("msg","不存在该用户!");
            out.println("<script>alert(\"不存在该用户!\");location.href = \"login.jsp\";</script>");
        }

//        PreparedStatement preparedStatement = connection.prepareStatement("select * from user where username = ? and password = ?;");
//        preparedStatement.setString(1,"admin");
//        preparedStatement.setString(2,"123456");
    } catch (Exception e) {
        throw new RuntimeException(e);
    }
%>

</body>
</html>

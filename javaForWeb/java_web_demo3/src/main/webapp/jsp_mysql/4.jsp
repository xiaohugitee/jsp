<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="com.red.utils.DriverConnection" %><%--
  Created by IntelliJ IDEA.
  User: humeng
  Date: 2022/11/2
  Time: 11:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>4</title>
</head>
<body>
<%
  request.setCharacterEncoding("utf-8");

  String name = request.getParameter("name");
  String price = request.getParameter("price");
  String category = request.getParameter("category");
  if ("".equals(name)||"".equals(price)|| "".equals(category)){
    out.println("<script>alert(\"不能为空\");location.href = \"3.jsp\";</script>");
    return;
  }

  try {
//    Class.forName("com.mysql.cj.jdbc.Driver");
//    Connection connection = DriverManager.getConnection("jdbc:mysql:///db1", "root", "12345678");
    Connection connection = DriverConnection.getConnection();
    PreparedStatement preparedStatement = connection.prepareStatement("insert into goods_info (name, price, category) values (?,?,?);");
    preparedStatement.setString(1,name);
    preparedStatement.setDouble(2, Double.parseDouble(price));
    preparedStatement.setString(3,category);
    int i = preparedStatement.executeUpdate();
    if (i>0){
      out.println("<script>alert(\"添加成功\");location.href = \"3.jsp\";</script>");
    }else {
      out.println("<script>alert(\"添加失败\");location.href = \"3.jsp\";</script>");
    }

  } catch (Exception e) {
    out.println("<script>alert(\"系统繁忙,请稍后再试\");location.href = \"3.jsp\";</script>");
    throw new RuntimeException(e);
  }
%>

</body>
</html>

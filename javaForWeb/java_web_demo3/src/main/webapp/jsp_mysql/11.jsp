<%@ page import="java.util.Objects" %>
<%@ page import="java.sql.*" %>
<%@ page import="com.red.utils.DriverConnection" %><%--
  Created by IntelliJ IDEA.
  User: humeng
  Date: 2022/11/7
  Time: 15:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>11</title>
</head>
<body>
<%
  request.setCharacterEncoding("utf-8");
  String username = request.getParameter("username");
  String password = request.getParameter("password");
  if ("".equals(username)||"".equals(password)|| Objects.isNull(username)||Objects.isNull(password)){
    out.println("<script>alert(\"用户名和密码不能为空\");location.href = \"login.jsp\";</script>");
    return;
  }
  try {
    Connection connection = DriverConnection.getConnection();
    PreparedStatement preparedStatement = connection.prepareStatement("select * from user where username = ?;");
    preparedStatement.setString(1,username);
    ResultSet resultSet = preparedStatement.executeQuery();
    if (resultSet.next()){
      session.setAttribute("msg","用户名重复");
      out.println("<script>alert(\"用户名重复\");location.href = \"register.jsp\";</script>");
    }else {
      PreparedStatement preparedStatement1 = connection.prepareStatement("insert into user (username, password) values (?,?);");
      preparedStatement1.setString(1,username);
      preparedStatement1.setString(2,password);
      int i = preparedStatement1.executeUpdate();

      if (i>0){
        session.setAttribute("username",username);
        session.removeAttribute("msg");
        out.println("<script>alert(\"注册成功!\");location.href = \"3.jsp\";</script>");

      }
    }
  } catch (Exception e) {
    throw new RuntimeException(e);
  }
%>


</body>
</html>

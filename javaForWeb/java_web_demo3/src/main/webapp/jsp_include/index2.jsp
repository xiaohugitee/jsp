<%--
  Created by IntelliJ IDEA.
  User: humeng
  Date: 2022/10/12
  Time: 10:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>jsp指令嵌套</title>
</head>
<body>
<%--嵌套--%>
<%--头部--%>
<%--<%@ include file="header.jsp"%>--%>
<%@ include file="header.jsp"%>
主体
<%@ include file="index.jsp"%>
<%--尾部--%>
<%@ include file="footer.jsp"%>


</body>
</html>

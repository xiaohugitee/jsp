<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.GregorianCalendar" %><%--
  Created by IntelliJ IDEA.
  User: humeng
  Date: 2022/10/12
  Time: 10:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>自动刷新</title>
</head>
<body>
<%
  // 设置每隔5秒刷新一次
  response.setIntHeader("Refresh", 5);
  // 获取当前时间
  Calendar calendar = new GregorianCalendar();
  String am_pm;
  int hour = calendar.get(Calendar.HOUR);
  int minute = calendar.get(Calendar.MINUTE);
  int second = calendar.get(Calendar.SECOND);
  if(calendar.get(Calendar.AM_PM) == 0)
    am_pm = "AM";
  else
    am_pm = "PM";
  String CT = hour+":"+ minute +":"+ second +" "+ am_pm;
  out.println("当前时间为: " + CT + "\n");
%>

</body>
</html>

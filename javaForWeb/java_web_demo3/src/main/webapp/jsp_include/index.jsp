<%@ page import="java.util.Date" %>
<%--
  Created by IntelliJ IDEA.
  User: humeng
  Date: 2022/10/12
  Time: 10:02
  To change this template use File | Settings | File Templates.
--%>
<%--jsp指令标签--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--jsp动作标签--%>
<%--<jsp:include page="header.jsp"/>--%>
<html>
<head>
    <title>index</title>
</head>
<body>
<h1>hello</h1>
<%--成员变量的定义以及方法声明--%>
<%!
    Date date = new Date();
%>
<%--java程序片--%>
<%
    String s = date.toLocaleString();
    out.println(s);
%>
@@@<br>
<%--java表达式--%>
<%=new Date()%>
<%=s%>


</body>
</html>

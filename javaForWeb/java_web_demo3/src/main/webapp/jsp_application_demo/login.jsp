<%--
  Created by IntelliJ IDEA.
  User: humeng
  Date: 2022/10/26
  Time: 14:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>login</title>
    <link rel="stylesheet" href="./css/common.css">
    <link rel="stylesheet" href="./css/login.css">
</head>
<body>

<div class="container">
    <img class="avatar" src="./images/photo_80.jpeg" alt="">
  <form action="./loginVerify.jsp" method="post">
    <input class="inp" type="text" name="username" placeholder="用户名" autocomplete="off">
    <input class="btn_sub" type="submit" value="ok">
  </form>
</div>


</body>
</html>

<%@ page import="java.util.Objects" %>
<%--
  Created by IntelliJ IDEA.
  User: humeng
  Date: 2022/10/26
  Time: 14:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>chatRoom</title>
    <link rel="stylesheet" href="./css/common.css">
    <link rel="stylesheet" href="./css/chatRoom.css">
</head>
<body>
<%
    response.setIntHeader("refresh",20);

    String username = (String) session.getAttribute("username");

    if (Objects.isNull(username)){
        //用户没有登陆 让用户登陆
        out.println("<script>alert(\"登陆已失效\");location.href=\"login.jsp\"</script>");
    }
%>
<div class="container">
<div class="body">
    <%
        String messageBody = (String) application.getAttribute("messageBody");
        out.println(messageBody==null?"":messageBody);
    %>
</div>
    <div class="inp_area">
        <form class="upload" action="./addMessage.jsp" method="post">
            <textarea type="text" class="message" name="message" placeholder="请输入内容来聊天吧~"></textarea>
            <input type="submit" class="ok" value="发送">
        </form>
    </div>
</div>

<script>
    window.onload=() => {
        <%--    让滚动条在最底部--%>
        const body = document.querySelector(".body");
        body.scrollTo(0,body.scrollHeight);
        const message = document.querySelector(".message");
        message.focus();

    }

</script>

</body>
</html>

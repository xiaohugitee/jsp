<%@ page import="java.util.Objects" %><%--
  Created by IntelliJ IDEA.
  User: humeng
  Date: 2022/10/26
  Time: 14:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>loginVerify</title>
</head>
<body>
<%
    request.setCharacterEncoding("utf-8");

    String username = request.getParameter("username");
    //如果没有输入用户名 弹出提示框,并跳转登录页
    if ("".equals(username)|| Objects.isNull(username)){
        out.println("<script>alert(\"请输入用户名\");location.href=\"login.jsp\"</script>");
        return;
    }
    //把用户名保存到session中
    session.setAttribute("username",username);
    response.sendRedirect("chatRoom.jsp");

%>


</body>
</html>

<%@ page import="java.util.Objects" %><%--
  Created by IntelliJ IDEA.
  User: humeng
  Date: 2022/10/26
  Time: 14:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>addMessage</title>
</head>
<body>

<%
    request.setCharacterEncoding("utf-8");
    String messageBody = (String) application.getAttribute("messageBody");

    if (messageBody==null){
        messageBody="";
    }

    String message = request.getParameter("message");
    String username = (String) session.getAttribute("username");
    if ("".equals(message) || Objects.isNull(message) || Objects.isNull(username)){
        response.sendRedirect("chatRoom.jsp");
        return;
    }

    application.setAttribute("messageBody",
            messageBody+"<br>"+"<div class=\"line\">" +
            "        <img class=\"avatar\" src=\"./images/photo_80.jpeg\" alt=\"\">\n" +
             username+":"+message+"</div>");
    response.sendRedirect("chatRoom.jsp");

%>

</body>
</html>

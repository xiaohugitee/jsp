import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;


public class TestConnectionMySQL {
    public static void main(String[] args) throws Exception {
        Class.forName("com.mysql.cj.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/db1?useUnicode=true&characterEncoding=utf8";
        String username = "root";
        String password = "12345678";
        Connection connection = DriverManager.getConnection(url, username, password);

        Statement statement = connection.createStatement();
        ResultSet executeQuery = statement.executeQuery("select * from user;");
        ArrayList<User> users = new ArrayList<>();
        while (executeQuery.next()){
            String username1 = executeQuery.getString("username");
            String password1 = executeQuery.getString("password");
            users.add(new User(username1,password1));
        }
        System.out.println(users);

    }
}

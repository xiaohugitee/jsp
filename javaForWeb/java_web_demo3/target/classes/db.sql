show tables ;

use db1;

create table user(
                     username varchar(30) not null comment '用户名',
                     password varchar(30) not null comment '密码'
) comment '用户表';

insert into user (username, password)
values ('admin','123456');

insert into user (username, password)
values ('张三','123');

select *
from user;

insert into user (username, password) values ('zs','123');

update user set username = 'zs' where password = '123';

delete from user where username = 'zs';

select * from user;
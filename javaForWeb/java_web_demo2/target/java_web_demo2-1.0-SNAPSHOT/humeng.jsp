<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%--
  Created by IntelliJ IDEA.
  User: humeng
  Date: 2022/10/10
  Time: 18:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>计算器</title>
    <link rel="stylesheet" href="./css/index.css">
    <script src="./js/index.js"></script>
</head>
<body>
<%!
    Date date = new Date();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
%>
<%
    String time = simpleDateFormat.format(date);

%>

<div class="container">
    <div class="header_show">
        <div class="show">
            <span class="time" id="time">
                <%--<%=time%>--%>
            </span>
            <span class="close"></span>
            <span class="min"></span>
            <span class="max"></span>
            <%--显示区域--%>
            <span class="show_calc">
                <input type="text" id="result" value="0">
            </span>
        </div>
    </div>
    <div class="body_btn">
        <div class="first_row f1">
            <div class="clear btn_dark f1" id="clear">AC</div>
            <div class="plus_or_minus btn_dark f1" id="plus_or_minus"><sup>+</sup>/<sub>-</sub></div>
            <div class="percent btn_dark f1" id="percent">%</div>
            <div class="div btn_calc f1" id="div">÷</div>
        </div>
        <div class="second_row f1">
            <div class="btn_number f1" id="seven">7</div>
            <div class="btn_number f1" id="eight">8</div>
            <div class="btn_number f1" id="nine">9</div>
            <div class="btn_calc f1" id="multiply">×</div>
        </div>
        <div class="third_row f1">
            <div class="btn_number f1" id="four">4</div>
            <div class="btn_number f1" id="five">5</div>
            <div class="btn_number f1" id="six">6</div>
            <div class="btn_calc f1" id="sub">-</div>
        </div>
        <div class="fourth_row f1">
            <div class="btn_number f1" id="one">1</div>
            <div class="btn_number f1" id="two">2</div>
            <div class="btn_number f1" id="three">3</div>
            <div class="btn_calc f1" id="adds">+</div>
        </div>
        <div class="fifth_row f1">
            <div class="btn_number f2" id="zero">0</div>
            <div class="btn_number f1" id="point">.</div>
            <div class="btn_calc f1" id="equal">=</div>
        </div>
    </div>

</div>


</body>
</html>

window.onload = function () {
    let result = document.querySelector('#result');
    let time = document.querySelector('#time');

    let clear = document.querySelector('#clear');
    let plus_or_minus = document.querySelector('#plus_or_minus');
    let percent = document.querySelector('#percent');
    let div = document.querySelector('#div');

    let seven = document.querySelector('#seven');
    let eight = document.querySelector('#eight');
    let nine = document.querySelector('#nine');
    let multiply = document.querySelector('#multiply');

    let four = document.querySelector('#four');
    let five = document.querySelector('#five');
    let six = document.querySelector('#six');
    let sub = document.querySelector('#sub');

    let one = document.querySelector('#one');
    let two = document.querySelector('#two');
    let three = document.querySelector('#three');
    let adds = document.querySelector('#adds');

    let zero = document.querySelector('#zero');
    let point = document.querySelector('#point');
    let equal = document.querySelector('#equal');

    setInterval(function () {
        let date = new Date();
        time.innerHTML = date.toLocaleString();
    }, 1000);

    result.addEventListener('keydown', function (event) {
        if (event.keyCode === 13) {
            equal.click();
        }

    })

    clear.addEventListener('click', function (e) {
        result.value = '0';
    })
    plus_or_minus.addEventListener('click', function (e) {
        if (result.value === '0') {
            result.value = '0';
        } else {
            result.value = -result.value;
        }
    })
    percent.addEventListener('click', function (e) {

        result.value = result.value / 100;


    })
    div.addEventListener('click', function (e) {

        result.value = result.value + '/';


    })
    seven.addEventListener('click', function (e) {
        if (result.value === '0') {
            result.value = '7';
        } else {
            result.value = result.value + '7';
        }
    })
    eight.addEventListener('click', function (e) {
        if (result.value === '0') {
            result.value = '8';
        } else {
            result.value = result.value + '8';
        }
    })
    nine.addEventListener('click', function (e) {
        if (result.value === '0') {
            result.value = '9';
        } else {
            result.value = result.value + '9';
        }
    })
    multiply.addEventListener('click', function (e) {
        result.value = result.value + '*';

    })
    four.addEventListener('click', function (e) {
        if (result.value === '0') {
            result.value = '4';
        } else {
            result.value = result.value + '4';
        }
    })

    five.addEventListener('click', function (e) {
        if (result.value === '0') {
            result.value = '5';
        } else {
            result.value = result.value + '5';
        }
    })
    six.addEventListener('click', function (e) {
        if (result.value === '0') {
            result.value = '6';
        } else {
            result.value = result.value + '6';
        }
    })
    sub.addEventListener('click', function (e) {
        result.value = result.value + '-';


    })
    one.addEventListener('click', function (e) {
        if (result.value === '0') {
            result.value = '1';
        } else {
            result.value = result.value + '1';
        }
    })
    two.addEventListener('click', function (e) {
        if (result.value === '0') {
            result.value = '2';
        } else {
            result.value = result.value + '2';
        }
    })
    three.addEventListener('click', function (e) {
        if (result.value === '0') {
            result.value = '3';
        } else {
            result.value = result.value + '3';
        }
    })
    adds.addEventListener('click', function (e) {
        result.value = result.value + '+';

    })
    zero.addEventListener('click', function (e) {
        if (result.value === '0') {
            result.value = '0';
        } else {
            result.value = result.value + '0';
        }
    })
    point.addEventListener('click', function (e) {
        if (result.value === '0') {
            result.value = '0';
        }
        //查找字符串中是否有小数点
        if (result.value.indexOf('.') === -1) {
            result.value = result.value + '.';
        }

    })
    equal.addEventListener('click', function (e) {
        //eval() 函数可计算某个字符串，并执行其中的的 JavaScript 代码。
        //解决乘除法运算精度问题
        result.value = eval(result.value).toFixed(8);
        //去除小数点后面的0
        result.value = parseFloat(result.value);


    })


}
window.onload = () => {
    const username = document.querySelector('#username')
    const phone = document.querySelector('#phone')

    const left = document.querySelector('#left')
    const right = document.querySelector('#right')
    const time = document.querySelector('#time')

    setInterval(() => {
        let date = new Date()
        let year = date.getFullYear()
        let month = date.getMonth() + 1
        let day = date.getDate()
        let hour = date.getHours()
        let minute = date.getMinutes()
        let second = date.getSeconds()
        if (month < 10) {
            month = '0' + month
        }
        if (day < 10) {
            day = '0' + day
        }
        if (hour < 10) {
            hour = '0' + hour
        }
        if (minute < 10) {
            minute = '0' + minute
        }
        if (second < 10) {
            second = '0' + second
        }
        time.innerHTML = `${year}年${month}月${day}日 ${hour}:${minute}:${second}`
    }, 16)


    username.onblur = function () {
        //边框变绿
        username.style.border = '1px solid green'
    }
    phone.onfocus = function () {
        left.style.backgroundImage = 'url("./images/22_close.0efad8c4.png")'
        right.style.backgroundImage = 'url("./images/33_close.eea03c39.png")'
    }
    phone.onblur = function () {
        //边框变绿
        phone.style.border = '1px solid green'
        left.style.backgroundImage = 'url("./images/22_open.72c00877.png")'
        right.style.backgroundImage = 'url("./images/33_open.43a09438.png")'

    }
    //
    // sub.addEventListener('click', (e) => {
    //     //阻止表单默认事件
    //     e.preventDefault()
    //
    // })


}
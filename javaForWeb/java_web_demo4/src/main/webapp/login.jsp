<%--
  Created by IntelliJ IDEA.
  User: humeng
  Date: 2022/10/17
  Time: 15:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>login</title>
</head>
<link rel="stylesheet" href="./css/1.css">
<script src="./js/1.js"></script>
<body>
<div class="container">
    <form class="register" action="verify.jsp" method="post">
        <div class="usernameDiv">
            <label for="username" class="title2">用户名:</label>
            <input type="text" name="username" class="inp" id="username" autocomplete="off" placeholder="请输入用户名">
        </div>
        <div class="phoneDiv">
            <label for="phone" class="title2">密&nbsp;&nbsp;&nbsp;码:</label>
            <input type="password" name="password" class="inp" id="phone" autocomplete="off" placeholder="请输入密码">
        </div>
        <div class="submitBtn">
            <input class="btn" type="submit" id="sub" value="提交">
        </div>
        <div class="left" id="left">
        </div>
        <div class="right" id="right">
        </div>
        <div class="time" id="time"></div>
    </form>
</div>

</body>
</html>

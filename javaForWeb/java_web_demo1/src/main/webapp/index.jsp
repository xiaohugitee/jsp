<%--
  Created by IntelliJ IDEA.
  User: humeng
  Date: 2022/9/28
  Time: 11:11
  To change this template use File | Settings | File Templates.
--%>
<%--1.jsp指令标记--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--2.jsp动作标记--%>
<%--<jsp:include page=""></jsp:include>--%>
<%--3.成员变量及方法的声明--%>
<%!
    String name = "张三";

    void show() {

        System.out.println(name);

    }
%>


<html>
<head>
    <title>Title</title>
</head>
<body>

<h1>hello jsp</h1>
<h1>你好 jsp</h1>
<%--4.java的程序片--%>
<%
    System.out.println("hello jsp");
    int i = 3;
    show();
%>

<%--5.java表达式  out.print("内容")--%>
<%="hello" + i%>


</body>
</html>
